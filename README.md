# Project Title

A graphical user interface for minecraft overviewer

## Getting Started

Just run python setup.py install

### Prerequisites

This application depends on th python package automat

```
Give examples
```

### Installing

Just run python setup.py install

## License

This project is licensed under the CC0 License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Thanks to the creators of minecraft overviewer for their work

