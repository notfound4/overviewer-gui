import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class WorldOption(Gtk.Dialog):

    def __init__(self, parent, location, json):
        Gtk.Dialog.__init__(self, os.path.basename(location), parent,
                            0, (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                Gtk.STOCK_OK, Gtk.ResponseType.OK))
        self.set_default_size(500, 400)
        self.set_modal(True)

        area = self.get_content_area()
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        labelmap = Gtk.Label("Maps to render")
        box.pack_start(labelmap, False, False, 5)
        self.cave = Gtk.CheckButton.new_with_label("Cave")
        self.cave.set_active(json[location]["cave"])
        box.pack_start(self.cave, False, False, 5)
        if os.path.isdir(os.path.join(location, "DIM-1/region")):
            self.nether = Gtk.CheckButton.new_with_label("Nether")
            self.nether.set_active(json[location]["nether"])
            box.pack_start(self.nether, False, False, 5)
        else:
            self.nether = None
        if os.path.isdir(os.path.join(location, "DIM1/region")):
            self.end = Gtk.CheckButton.new_with_label("End")
            self.end.set_active(json[location]["end"])
            box.pack_start(self.end, False, False, 5)
        else:
            self.end = None

        label = Gtk.Label("Layers")
        box.pack_start(label, False, False, 5)
        self.biome = Gtk.CheckButton.new_with_label("Biome layer")
        self.biome.set_active(json[location]["biome"])
        box.pack_start(self.biome, False, False, 5)
        self.slime = Gtk.CheckButton.new_with_label("Slime")
        self.slime.set_active(json[location]["slime"])
        box.pack_start(self.slime, False, False, 5)

        labelMark = Gtk.Label("Markers for " + os.path.basename(location))
        box.pack_start(labelMark, False, False, 5)
        self.players = Gtk.CheckButton.new_with_label("Players")
        self.players.set_active(json[location]["players"])
        box.pack_start(self.players, False, False, 5)
        self.spawners = Gtk.CheckButton.new_with_label("Spawners")
        self.spawners.set_active(json[location]["spawners"])
        box.pack_start(self.spawners, False, False, 5)
        self.chests = Gtk.CheckButton.new_with_label("Chests")
        self.chests.set_active(json[location]["chests"])
        box.pack_start(self.chests, False, False, 5)

        area.add(box)
        self.show_all()

    def is_biome_active(self):
        return(self.biome.get_active())

    def is_cave_active(self):
        return self.cave.get_active()

    def is_slime_active(self):
        return self.slime.get_active()

    def is_players_active(self):
        return self.players.get_active()

    def is_spawners_active(self):
        return self.spawners.get_active()

    def is_chests_active(self):
        return self.chests.get_active()

    def is_nether_active(self):
        return self.nether is None or self.nether.get_active()

    def is_end_active(self):
        return self.end is None or self.end.get_active()
