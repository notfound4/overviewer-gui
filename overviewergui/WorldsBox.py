import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from WorldItem import WorldItem
from WorldOption import WorldOption


class WorldsBox(Gtk.Box):

    def __init__(self, parent):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.parent = parent
        self.dict = dict()
        self.json = dict()

        label = Gtk.Label("Select the worlds to render",
                          justify=Gtk.Justification.LEFT, xalign=0)
        self.pack_start(label, False, False, 0)

        self.listboxDir = Gtk.ListBox()
        self.listboxDir.set_selection_mode(Gtk.SelectionMode.NONE)
        self.pack_start(self.listboxDir, True, True, 0)

    def add(self, name, active):
        self.dict[name] = Gtk.ListBoxRow()
        hbox = WorldItem(name)
        self.dict[name].add(hbox)
        self.listboxDir.add(self.dict[name])
        hbox.connect("modify_signal", self.on_modify)
        self.dict[name].show_all()
        hbox.set_active(active)

        if name in self.parent.folderConfig.worlds:
            self.json[name] = self.parent.folderConfig.worlds[name]
        else:
            self.json[name] = {"enabled": active, "biome": True, "cave": True,
                               "slime": True, "players": True, "spawners": True,
                               "chests": True, "nether": True, "end": True}

    def on_modify(self, widget, location):
        dialog = WorldOption(self.parent, location, self.json)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.json[location]["biome"] = dialog.is_biome_active()
            self.json[location]["cave"] = dialog.is_cave_active()
            self.json[location]["slime"] = dialog.is_slime_active()
            self.json[location]["players"] = dialog.is_players_active()
            self.json[location]["chests"] = dialog.is_chests_active()
            self.json[location]["spawners"] = dialog.is_spawners_active()
            self.json[location]["nether"] = dialog.is_nether_active()
            self.json[location]["end"] = dialog.is_end_active()

        dialog.destroy()

    def reset(self):
        for b in self.listboxDir.get_children():
            self.listboxDir.remove(b)
        self.dict = dict()
        self.json = dict()

        folders = list()
        worlds = self.parent.folderConfig.worlds

        for d, c in self.parent.folderConfig.inputPaths.items():
            if os.path.isdir(d) and c:
                folders.append(d)

        for dir_main in folders:
            for d in filter(lambda x: os.path.isdir(x) and os.path.isfile(os.path.join(x, "level.dat")),
                            [os.path.join(dir_main, s) for s in os.listdir(dir_main)]):
                self.add(d, d not in worlds or worlds[d]["enabled"])

    def commitChange(self):
        for b in self.listboxDir.get_children():
            self.json[b.get_children()[0].location]["enabled"] = b.get_children()[0].is_active()
        self.parent.folderConfig.worlds = self.json
