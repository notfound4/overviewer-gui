import re
import gi
from threading import Thread
gi.require_version('Gtk', '3.0')
from gi.repository import GObject


class RenderThread(Thread):

    def __init__(self, pipe, parent):
        Thread.__init__(self)
        self.pattern = re.compile("^([0-9]+) ([0-9]+) ([0-9]+\.?[0-9]*) (.*)$")
        self.parent = parent
        self.pipe = pipe
        self.fraction = 0.0
        self.progress = ""
        self.text = ""

    def run(self):
        try:
            with self.pipe:
                for line in iter(self.pipe.readline, b''):
                    strLine = line.decode("utf-8").replace("\n", "")
                    if self.pattern.match(strLine):
                        list_var = self.pattern.split(strLine)
                        n_done = list_var[1]
                        n_tot = list_var[2]
                        perc = list_var[3]
                        eta = list_var[4]
                        self.fraction = float(perc) / 100.0
                        self.progress = "%.2f" % float(perc) + " % (" + n_done + " / " + n_tot + "), eta: " + eta
                        GObject.idle_add(self.parent.update_progress)
                    else:
                        self.text = strLine + "\n"
                        GObject.idle_add(self.parent.update_text)
        finally:
            GObject.idle_add(self.parent.render_done)
