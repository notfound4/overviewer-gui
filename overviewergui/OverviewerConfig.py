import os
import json
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib


class OverviewerConfig:
    """docstring for Config"""
    def __init__(self):

        # Config file definition
        self.confDir = os.path.join(GLib.get_user_config_dir(), 'minecraft-overviewer/')
        self.confFile = os.path.join(self.confDir + "config")

        # Loading defaults
        self.outputPath = os.path.join(GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DOCUMENTS),
                                       'minecraft-overviewer/')

        # Load Json if available or create it otherwise
        if self.exists():
            self.load()
        else:
            self.create()

    def load(self):
        if os.path.isfile(self.confFile):
            with open(self.confFile) as json_data_file:
                data = json.load(json_data_file)
                if 'output' in data:
                    self.outputPath = data['output']

    def save(self):
        with open(self.confFile) as json_data_file:
            data = json.load(json_data_file)
            data['output'] = self.outputPath
        with open(self.confFile, 'w') as outfile:
            json.dump(data, outfile, indent=4, sort_keys=True)

    def create(self):
        if not os.path.isfile(self.confFile):
            if not os.path.exists(self.confDir):
                os.makedirs(self.confDir)
            with open(self.confFile, 'w') as outfile:
                json.dump({}, outfile, indent=4, sort_keys=True)
            self.save()

    def exists(self):
        return os.path.isfile(self.confFile)
