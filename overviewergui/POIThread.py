import gi
from threading import Thread
gi.require_version('Gtk', '3.0')
from gi.repository import GObject


class POIThread(Thread):

    def __init__(self, pipe, parent):
        Thread.__init__(self)
        self.parent = parent
        self.pipe = pipe
        self.text = ""

    def run(self):
        try:
            with self.pipe:
                for line in iter(self.pipe.readline, b''):
                    self.text = line.decode("utf-8")
                    GObject.idle_add(self.parent.update_text)
        finally:
            GObject.idle_add(self.parent.genpoi_done)
