import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class OutputBox(Gtk.Box):

    def __init__(self, parent):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.parent = parent

        label = Gtk.Label("Choose output directory", justify=Gtk.Justification.LEFT, xalign=0)
        self.pack_start(label, False, False, 0)

        folder_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.pack_start(folder_box, True, False, 0)

        self.entry = Gtk.Entry(text=self.parent.config.outputPath)
        folder_box.pack_start(self.entry, True, True, 0)

        image = Gtk.Image(stock=Gtk.STOCK_OPEN)
        button = Gtk.Button(image=image)
        folder_box.pack_end(button, False, False, 0)
        button.connect("clicked", self.on_choose)

    def on_choose(self, widget):
        dialog = Gtk.FileChooserDialog("Please choose a folder", self.parent,
                                       Gtk.FileChooserAction.SELECT_FOLDER,
                                       (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, "Select", Gtk.ResponseType.OK),
                                       create_folders=True)
        dialog.set_default_size(800, 400)
        dialog.set_modal(True)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.entry.set_text(dialog.get_filename())

        dialog.destroy()

    def commitChange(self):
        self.parent.config.outputPath = self.entry.get_text()
