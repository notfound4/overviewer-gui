import os
import json
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib
from pkg_resources import resource_stream


class OverviewerFolderConfig:
    """docstring for Config"""
    def __init__(self, location):

        # Config file definition
        self.confDir = location + "//"
        self.confFile = os.path.join(self.confDir + "overviewer.conf")

        # Loading defaults
        self.inputPaths = {os.path.join(GLib.get_home_dir(), '.minecraft/saves/'): True}
        self.worlds = {}
        self.texture = "Default"

        # Load Json if available or create it otherwise
        if self.exists():
            self.load()
        else:
            self.create()

    def load(self):
        if os.path.isfile(self.confFile):
            with open(self.confFile) as json_data_file:
                data = json.load(json_data_file)
                if 'inputs' in data:
                    self.inputPaths = data['inputs']
                if 'worlds' in data:
                    self.worlds = dict()
                    for w in data['worlds']:
                        d = data['worlds'][w]
                        if isinstance(d, dict):
                            self.worlds[w] = d
                        else:
                            self.worlds[w] = {"enabled": d, "biome": True, "cave": True,
                                              "slime": True, "players": True, "spawners": True,
                                              "chests": True, "nether": True, "end": True}
                if 'texture' in data:
                    self.texture = data['texture']

    def save(self):
        with open(self.confFile) as json_data_file:
            data = json.load(json_data_file)
            data['inputs'] = self.inputPaths
            data['worlds'] = self.worlds
            data['texture'] = self.texture
        with open(self.confFile, 'w') as outfile:
            json.dump(data, outfile, indent=4, sort_keys=True)

    def create(self):
        if not os.path.isfile(self.confFile):
            if not os.path.exists(self.confDir):
                os.makedirs(self.confDir)
            with open(self.confFile, 'w') as outfile:
                json.dump({}, outfile, indent=4, sort_keys=True)
            self.save()
        if not os.path.exists(os.path.join(self.confDir, "icons")):
            os.makedirs(os.path.join(self.confDir, "icons"))
        chest_path = os.path.join(self.confDir, "icons/chest.png")
        if not os.path.isfile(chest_path):
            chest_png = resource_stream(__name__, 'icons/chest.png')
            with open(chest_path, 'w') as chest_file:
                chest_file.write(chest_png.read())
                chest_file.close()
        spawner_path = os.path.join(self.confDir, "icons/spawner.png")
        if not os.path.isfile(spawner_path):
            spawner_png = resource_stream(__name__, 'icons/spawner.png')
            with open(spawner_path, 'w') as spawner_file:
                spawner_file.write(spawner_png.read())
                spawner_file.close()

    def exists(self):
        return os.path.isfile(self.confFile)

    def createMapFile(self):
        with open(os.path.join(self.confDir, "config.map"), 'w') as outfile:
            outfile.write("# -- coding: utf-8 --\n\n")
            outfile.write("def playerIcons(poi):" + "\n")
            outfile.write("    if poi['id'] == 'Player':" + "\n")
            outfile.write("        poi['icon'] = \"https://overviewer.org/avatar/%s\" % poi['EntityId']" + "\n")
            outfile.write("        return \"Last known location for %s\" % poi['EntityId']" + "\n\n")

            outfile.write("def spawnerFilter(poi):" + "\n")
            outfile.write("    if poi['id'] == 'MobSpawner' or poi['id'] == 'minecraft:mob_spawner':" + "\n")
            outfile.write("        if 'EntityId' in poi:" + "\n")
            outfile.write("            monster = poi['EntityId']" + "\n")
            outfile.write("        else:" + "\n")
            outfile.write("            monster = poi['SpawnPotentials'][0]['Entity']['id']" + "\n")
            outfile.write("        return \"[MobSpawner]\\n %s\\n found at X:%d, Y:%d, Z:%d\" % (monster, poi['x'], poi['y'], poi['z'])" + "\n\n")

            outfile.write("def chestFilter(poi):" + "\n")
            outfile.write("    if poi['id'] == 'Chest' or poi['id'] == 'minecraft:chest':" + "\n")
            outfile.write("        return \"Chest\"" + "\n\n\n")

            outfile.write("biomes = [ClearBase(), BiomeOverlay()]" + "\n")
            outfile.write("slimes = [ClearBase(), SlimeOverlay()]" + "\n")
            outfile.write("my_nether = [Base(), EdgeLines(), Nether(), SmoothLighting(strength=0.5)]" + "\n")
            outfile.write("my_end = [Base(), EdgeLines(), SmoothLighting(strength=0.5)]" + "\n\n\n")

            for w in self.worlds:
                if not self.worlds[w]["enabled"]:
                    continue
                outfile.write("worlds[\"" + os.path.basename(w) + "\"] = \"" + w + "\"\n")

                outfile.write("renders[\"" + os.path.basename(w) + "-overworld\"] = {" + "\n")
                outfile.write("    \"world\": \"" + os.path.basename(w) + "\"," + "\n")
                outfile.write("    \"title\": \"Overworld\"," + "\n")
                outfile.write("    \"rendermode\": smooth_lighting," + "\n")
                outfile.write("    \"markers\": [")
                if self.worlds[w]["players"]:
                    outfile.write("dict(name=\"Players\", filterFunction=playerIcons, checked=True)," + "\n                ")
                if self.worlds[w]["chests"]:
                    outfile.write("dict(name=\"Chests\", filterFunction=chestFilter, icon=\"icons/chest.png\", createInfoWindow=False)," + "\n                ")
                if self.worlds[w]["spawners"]:
                    outfile.write("dict(name=\"Spawners\", filterFunction=spawnerFilter, icon=\"icons/spawner.png\")")
                outfile.write("],\n")
                outfile.write("    \"dimension\": \"overworld\"" + "\n")
                outfile.write("}" + "\n\n")

                if self.worlds[w]["biome"]:
                    outfile.write("renders[\"" + os.path.basename(w) + "-biomes\"] = {" + "\n")
                    outfile.write("    \"world\": \"" + os.path.basename(w) + "\"," + "\n")
                    outfile.write("    \"title\": \"Biomes\"," + "\n")
                    outfile.write("    \"rendermode\": biomes," + "\n")
                    outfile.write("    \"overlay\": [\"" + os.path.basename(w) + "-overworld\"]," + "\n")
                    outfile.write("    \"dimension\": \"overworld\"" + "\n")
                    outfile.write("}" + "\n\n")

                if self.worlds[w]["slime"]:
                    outfile.write("renders[\"" + os.path.basename(w) + "-slimes\"] = {" + "\n")
                    outfile.write("    \"world\": \"" + os.path.basename(w) + "\"," + "\n")
                    outfile.write("    \"title\": \"Slimes\"," + "\n")
                    outfile.write("    \"rendermode\": slimes," + "\n")
                    outfile.write("    \"overlay\": [\"" + os.path.basename(w) + "-overworld\"]," + "\n")
                    outfile.write("    \"dimension\": \"overworld\"" + "\n")
                    outfile.write("}" + "\n\n")

                if self.worlds[w]["cave"]:
                    outfile.write("renders[\"" + os.path.basename(w) + "-cave\"] = {" + "\n")
                    outfile.write("    \"world\": \"" + os.path.basename(w) + "\"," + "\n")
                    outfile.write("    \"title\": \"Cave\"," + "\n")
                    outfile.write("    \"rendermode\": cave," + "\n")
                    outfile.write("    \"markers\": [")
                    if self.worlds[w]["players"]:
                        outfile.write("dict(name=\"Players\", filterFunction=playerIcons, checked=True)," + "\n                ")
                    if self.worlds[w]["chests"]:
                        outfile.write("dict(name=\"Chests\", filterFunction=chestFilter, icon=\"icons/chest.png\", createInfoWindow=False)," + "\n                ")
                    if self.worlds[w]["spawners"]:
                        outfile.write("dict(name=\"Spawners\", filterFunction=spawnerFilter, icon=\"icons/spawner.png\")")
                    outfile.write("],\n")
                    outfile.write("    \"dimension\": \"overworld\"" + "\n")
                    outfile.write("}" + "\n\n")

                if os.path.isdir(os.path.join(w, "DIM1/region")) and self.worlds[w]["end"]:
                    outfile.write("renders[\"" + os.path.basename(w) + "-end\"] = {" + "\n")
                    outfile.write("    \"world\": \"" + os.path.basename(w) + "\"," + "\n")
                    outfile.write("    \"title\": \"The End\"," + "\n")
                    outfile.write("    \"rendermode\": my_end," + "\n")
                    outfile.write("    \"markers\": [")
                    if self.worlds[w]["players"]:
                        outfile.write("dict(name=\"Players\", filterFunction=playerIcons, checked=True)," + "\n                ")
                    if self.worlds[w]["chests"]:
                        outfile.write("dict(name=\"Chests\", filterFunction=chestFilter, icon=\"icons/chest.png\", createInfoWindow=False)," + "\n                ")
                    if self.worlds[w]["spawners"]:
                        outfile.write("dict(name=\"Spawners\", filterFunction=spawnerFilter, icon=\"icons/spawner.png\")")
                    outfile.write("],\n")
                    outfile.write("    \"dimension\": \"DIM1\"" + "\n")
                    outfile.write("}" + "\n\n")

                if os.path.isdir(os.path.join(w, "DIM-1/region")) and self.worlds[w]["nether"]:
                    outfile.write("renders[\"" + os.path.basename(w) + "-nether\"] = {" + "\n")
                    outfile.write("    \"world\": \"" + os.path.basename(w) + "\"," + "\n")
                    outfile.write("    \"title\": \"Nether\"," + "\n")
                    outfile.write("    \"rendermode\": my_nether," + "\n")
                    outfile.write("    \"markers\": [")
                    if self.worlds[w]["players"]:
                        outfile.write("dict(name=\"Players\", filterFunction=playerIcons, checked=True)," + "\n                ")
                    if self.worlds[w]["chests"]:
                        outfile.write("dict(name=\"Chests\", filterFunction=chestFilter, icon=\"icons/chest.png\", createInfoWindow=False)," + "\n                ")
                    if self.worlds[w]["spawners"]:
                        outfile.write("dict(name=\"Spawners\", filterFunction=spawnerFilter, icon=\"icons/spawner.png\")")
                    outfile.write("],\n")
                    outfile.write("    \"dimension\": \"DIM-1\"" + "\n")
                    outfile.write("}" + "\n\n")

            outfile.write("\n\n")
            outfile.write("imgquality = 100" + "\n")
            outfile.write("outputdir = \"" + self.confDir + "\"" + "\n")
            if self.texture != "Default":
                outfile.write("texturepath = \"" + os.path.join(os.path.join(GLib.get_home_dir(), '.minecraft/resourcepacks/'), self.texture) + "\"" + "\n")
            outfile.write("\n")
            outfile.write("import time\n")
            outfile.write("import logging\n")
            outfile.write("from .observer import Observer\n")
            outfile.write("globals()['time'] = time\n")
            outfile.write("globals()['logging'] = logging\n")
            outfile.write("globals()['Observer'] = Observer\n\n")
            outfile.write("class ParsableObserver(Observer):\n")
            outfile.write("    \"\"\"Simple observer that output a percentage\"\"\"\n")
            outfile.write("    def __init__(self):\n")
            outfile.write("        self.format = lambda seconds: '%02ih %02im %02is' % (seconds // 3600, (seconds % 3600) // 60, seconds % 60)\n")
            outfile.write("        super(ParsableObserver, self).__init__()\n\n")
            outfile.write("    def start(self, max_value):\n")
            outfile.write("        super(ParsableObserver, self).start(max_value)\n")
            outfile.write("        logging.info(\"Rendering %d total tiles.\" % max_value)\n\n")
            outfile.write("    def finish(self):\n")
            outfile.write("        seconds_elapsed = time.time() - self.start_time\n")
            outfile.write("        print(self.get_max_value(), self.get_max_value(), 100, self.format(seconds_elapsed))\n")
            outfile.write("        super(ParsableObserver, self).finish()\n\n")
            outfile.write("    def update(self, current_value):\n")
            outfile.write("        super(ParsableObserver, self).update(current_value)\n")
            outfile.write("        seconds_elapsed = time.time() - self.start_time\n")
            outfile.write("        if super(ParsableObserver, self).is_finished():\n")
            outfile.write("            eta = self.format(seconds_elapsed)\n")
            outfile.write("        else:\n")
            outfile.write("            if self.get_current_value() == 0:\n")
            outfile.write("                eta = \"NA\"\n")
            outfile.write("            else:\n")
            outfile.write("                eta = self.format(seconds_elapsed * (self.get_max_value() - self.get_current_value()) / self.get_current_value())\n")
            outfile.write("        print(self.get_current_value(), self.get_max_value(), self.get_percentage(), eta)\n\n")
            outfile.write("globals()['ParsableObserver'] = ParsableObserver\n\n")
            outfile.write("observer = ParsableObserver()\n")
