import os
import gi
import webbrowser
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from subprocess import Popen, PIPE
from automat import MethodicalMachine
from RenderThread import RenderThread
from POIThread import POIThread


class RenderBox(Gtk.Box):

    _machine = MethodicalMachine()

    def __init__(self, parent):
        super(RenderBox, self).__init__(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.parent = parent

        labelDir = Gtk.Label("Rendering maps", justify=Gtk.Justification.LEFT, xalign=0)
        self.pack_start(labelDir, False, False, 0)

        self.scroller = Gtk.ScrolledWindow()

        text = Gtk.TextView()
        text.set_editable(False)
        text.set_cursor_visible(False)
        text.set_justification(Gtk.Justification.LEFT)
        self.textbuffer = text.get_buffer()

        self.scroller.add(text)
        self.pack_start(self.scroller, True, True, 5)

        self.progressbar = Gtk.ProgressBar()
        self.progressbar.set_show_text(True)
        self.pack_start(self.progressbar, False, False, 5)

        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.webBut = Gtk.Button("Open in web browser")
        self.closeBut = Gtk.Button("Close")
        self.webBut.connect("clicked", self.open_maps)
        self.closeBut.connect("clicked", self.quit)
        hbox.pack_start(self.webBut, False, False, 5)
        hbox.pack_end(self.closeBut, False, False, 5)
        self.pack_start(hbox, False, False, 0)

        self.process = None
        self.thread = None

    def update_progress(self):
        self.progressbar.set_fraction(self.thread.fraction)
        self.progressbar.set_text(self.thread.progress)
        return False

    def update_text(self):
        iter_ = self.textbuffer.get_end_iter()
        self.textbuffer.insert(iter_, self.thread.text)
        va = self.scroller.get_vadjustment()
        va.set_value(va.get_upper())

    # ---- State Machine  ---- #

    # -- Outputs -- #

    @_machine.output()
    def render_output(self, widget=None):

        self.process = Popen(["overviewer.py",
                              "--config=" + os.path.join(self.parent.folderConfig.confDir, "config.map")],
                             stdout=PIPE, bufsize=1)
        self.thread = RenderThread(self.process.stdout, self)
        self.thread.start()

    @_machine.output()
    def render_poi(self, widget=None):

        self.process = Popen(["overviewer.py", "--genpoi",
                              "--config=" + os.path.join(self.parent.folderConfig.confDir, "config.map")],
                             stdout=PIPE, bufsize=1)
        self.thread = POIThread(self.process.stdout, self)
        self.thread.start()

    @_machine.output()
    def init_widgets(self, widget=None):
        self.textbuffer.set_text("")
        self.progressbar.set_text("0 %")
        self.progressbar.set_fraction(0.0)

    @_machine.output()
    def activate_buttons(self, widget=None):
        self.webBut.set_sensitive(True)
        self.closeBut.set_sensitive(True)

    @_machine.output()
    def deactivate_buttons(self, widget=None):
        self.webBut.set_sensitive(False)
        self.closeBut.set_sensitive(False)

    @_machine.output()
    def open_maps_output(self, widget=None):
        webbrowser.open('file://' + os.path.join(self.parent.folderConfig.confDir + "index.html"))

    @_machine.output()
    def destroy_window(self, widget=None):
        self.parent.destroy()

    @_machine.output()
    def kill_subprocess(self, widget=None):
        if self.process is not None:
            if self.process.poll() is None:
                self.process.kill()
            self.process.wait()
            self.process = None
        if self.thread is not None:
            self.thread.join()
            self.thread = None

    # -- Inputs -- #

    @_machine.input()
    def render_worlds(self, widget=None):
        "Render selected worlds"

    @_machine.input()
    def render_done(self, widget=None):
        "Finished rendering worlds"

    @_machine.input()
    def genpoi_done(self, widget=None):
        "Finished rendering poi"

    @_machine.input()
    def quit(self, widget=None):
        "Quit"

    @_machine.input()
    def open_maps(self, widget=None):
        "Open the maps then quit"

    # -- States -- #

    @_machine.state(initial=True)
    def waiting(self):
        "Waiting to render"

    @_machine.state()
    def rendering(self):
        "Rendering the worlds"

    @_machine.state()
    def poi(self):
        "Adding the poi"

    @_machine.state()
    def finished(self):
        "Rendering finished"

    # -- Transitions -- #

    waiting.upon(render_worlds, enter=rendering,
                 outputs=[deactivate_buttons, init_widgets, render_output])

    rendering.upon(render_done, enter=poi,
                   outputs=[kill_subprocess, render_poi])

    poi.upon(genpoi_done, enter=finished,
             outputs=[kill_subprocess, activate_buttons])

    finished.upon(open_maps, enter=finished,
                  outputs=[open_maps_output, destroy_window])

    finished.upon(quit, enter=finished,
                  outputs=[destroy_window])
    waiting.upon(quit, enter=waiting,
                 outputs=[destroy_window])
    rendering.upon(quit, enter=rendering,
                   outputs=[kill_subprocess, destroy_window])
    poi.upon(quit, enter=poi,
             outputs=[kill_subprocess, destroy_window])
