__version__ = '0.1.0'

__all__ = ["main"]


def exit_main(arg):
	import gi
	gi.require_version('Gtk', '3.0')
	from gi.repository import Gtk
	arg.on_exit()
	Gtk.main_quit(arg)


def main():
	import gi
	gi.require_version('Gtk', '3.0')
	from gi.repository import Gtk
	import OverviewerWindow
	win = OverviewerWindow.MyWindow()
	win.connect("destroy", exit_main)
	win.show()
	Gtk.main()
