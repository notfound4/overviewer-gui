import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from automat import MethodicalMachine
from OverviewerConfig import OverviewerConfig
from OverviewerFolderConfig import OverviewerFolderConfig
from OutputBox import OutputBox
from InputsBox import InputsBox
from WorldsBox import WorldsBox
from TextureBox import TextureBox
from RenderBox import RenderBox


def get_resource_path(rel_path):
    dir_of_py_file = os.path.dirname(__file__)
    rel_path_to_resource = os.path.join(dir_of_py_file, rel_path)
    abs_path_to_resource = os.path.abspath(rel_path_to_resource)
    return abs_path_to_resource


class MyWindow(Gtk.Window):

    _machine = MethodicalMachine()

    def __init__(self):
        Gtk.Window.__init__(self, title="Minecraft Overviewer")
        self.set_border_width(10)
        self.set_default_size(640, 520)

        icon_name = "overviewer-gui"
        icon_theme = Gtk.IconTheme.get_default()

        found_icons = set()
        for res in range(0, 512, 2):
            icon = icon_theme.lookup_icon(icon_name, res, 0)
            if icon is not None:
                found_icons.add(icon.get_filename())

        if len(found_icons) > 0:
            sizes = Gtk.IconTheme.get_default().get_icon_sizes(icon_name)
            max_size = max(sizes)
            pixbuf = icon_theme.load_icon(icon_name, max_size, 0)
            self.set_default_icon_list([pixbuf])

        self.mainBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        buttonBox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.prevButton = Gtk.Button("Previous")
        self.nextButton = Gtk.Button("Next")
        buttonBox.pack_start(self.prevButton, False, False, 0)
        buttonBox.pack_end(self.nextButton, False, False, 0)
        self.prevButton.connect("clicked", self.prev)
        self.nextButton.connect("clicked", self.next)
        self.mainBox.pack_end(buttonBox, False, False, 0)
        self.nextButton.set_can_default(True)

        self.add(self.mainBox)

        self.config = OverviewerConfig()
        self.folderConfig = None
        self.outputSelection = OutputBox(self)
        self.inputsSelection = InputsBox(self)
        self.worldsSelection = WorldsBox(self)
        self.textureSelection = TextureBox(self)
        self.renderBox = RenderBox(self)
        self.current = self.outputSelection
        self.mainBox.pack_start(self.current, True, True, 0)

        self.show_all()
        self.prevButton.hide()

        self.set_default(self.nextButton)
        self.nextButton.grab_default()
        self.nextButton.grab_focus()

    def on_exit(self):
        self.renderBox.quit()

    # ---- State Machine  ---- #

    # -- Outputs Box -- #

    @_machine.output()
    def closeCurrent(self, widget):
        if self.current is not None:
            self.mainBox.remove(self.current)
            self.current.hide()
            self.current = None

    @_machine.output()
    def commitCurrent(self, widget):
        self.current.commitChange()

    @_machine.output()
    def openOutputSelection(self, widget):
        self.current = self.outputSelection
        self.mainBox.pack_start(self.current, True, True, 0)
        self.current.show_all()

        self.set_default(self.nextButton)
        self.nextButton.grab_default()
        self.nextButton.grab_focus()

    @_machine.output()
    def openInputsSelection(self, widget):
        self.current = self.inputsSelection
        self.current.reset()
        self.mainBox.pack_start(self.current, True, True, 0)
        self.current.show_all()

        self.set_default(self.nextButton)
        self.nextButton.grab_default()
        self.nextButton.grab_focus()

    @_machine.output()
    def openWorldsSelection(self, widget):
        self.current = self.worldsSelection
        self.current.reset()
        self.mainBox.pack_start(self.current, True, True, 0)
        self.current.show_all()

        self.set_default(self.nextButton)
        self.nextButton.grab_default()
        self.nextButton.grab_focus()

    @_machine.output()
    def openTextureSelection(self, widget):
        self.current = self.textureSelection
        self.current.reset()
        self.mainBox.pack_start(self.current, True, True, 0)
        self.current.show_all()

        self.set_default(self.nextButton)
        self.nextButton.grab_default()
        self.nextButton.grab_focus()

    @_machine.output()
    def openRenderBox(self, widget):
        self.current = self.renderBox
        self.current.render_worlds()
        self.mainBox.pack_start(self.current, True, True, 0)
        self.current.show_all()

    # -- Outputs Utility -- #

    @_machine.output()
    def clearFolderConfig(self, widget):
        self.folderConfig = None

    @_machine.output()
    def createFolderConfig(self, widget):
        self.folderConfig = OverviewerFolderConfig(self.config.outputPath)

    @_machine.output()
    def hidePrev(self, widget):
        self.prevButton.hide()

    @_machine.output()
    def hideNext(self, widget):
        self.nextButton.hide()

    @_machine.output()
    def showPrev(self, widget):
        self.prevButton.show()

    @_machine.output()
    def showNext(self, widget):
        self.nextButton.show()

    @_machine.output()
    def saveConfigs(self, widget):
        self.config.save()
        self.folderConfig.save()

    @_machine.output()
    def createMapFile(self, widget):
        self.folderConfig.createMapFile()

    # -- Inputs -- #

    @_machine.input()
    def prev(self, widget):
        "\"Previous\" button clicked"

    @_machine.input()
    def next(self, widget):
        "\"Next\" button clicked"

    # -- States -- #

    @_machine.state(initial=True)
    def output_selection(self):
        "In this state, the user is choosing the output directory"

    @_machine.state()
    def inputs_selection(self):
        "In this state, the user is choosing the input directories"

    @_machine.state()
    def worlds_selection(self):
        "In this state, the user is choosing the worlds to render"

    @_machine.state()
    def texture_selection(self):
        "In this state, the user is choosing the texture pack to use"

    @_machine.state()
    def rendering(self):
        "In this state, the maps are rendering"

    @_machine.state()
    def finished(self):
        "The end !"

    # -- Transitions -- #

    output_selection.upon(next, enter=inputs_selection,
                          outputs=[commitCurrent, closeCurrent, createFolderConfig,
                                   openInputsSelection, showPrev])
    inputs_selection.upon(prev, enter=output_selection,
                          outputs=[closeCurrent, openOutputSelection,
                                   hidePrev, clearFolderConfig])
    inputs_selection.upon(next, enter=worlds_selection,
                          outputs=[commitCurrent, closeCurrent,
                                   openWorldsSelection])
    worlds_selection.upon(prev, enter=inputs_selection,
                          outputs=[closeCurrent, openInputsSelection])
    worlds_selection.upon(next, enter=texture_selection,
                          outputs=[commitCurrent, closeCurrent,
                                   openTextureSelection])
    texture_selection.upon(prev, enter=worlds_selection,
                           outputs=[closeCurrent, openWorldsSelection])
    texture_selection.upon(next, enter=rendering,
                           outputs=[commitCurrent, closeCurrent,
                                    saveConfigs, createMapFile, hideNext, hidePrev,
                                    openRenderBox])
