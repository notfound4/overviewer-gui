import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GObject


class WorldItem(Gtk.Box):
    __gsignals__ = {
        'modify_signal': (GObject.SIGNAL_RUN_FIRST, None, (str,))
    }

    def __init__(self, location):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL, spacing=6)

        self.location = location

        self.check = Gtk.CheckButton.new_with_label(os.path.basename(location))
        self.pack_start(self.check, True, True, 5)

        if os.path.isfile(os.path.join(self.location, "icon.png")):
            image = Gtk.Image()
            image.set_from_file(os.path.join(self.location, "icon.png"))
            self.pack_end(image, False, False, 5)

        image = Gtk.Image(stock=Gtk.STOCK_EDIT)
        self.button = Gtk.Button(image=image)
        self.pack_end(self.button, False, False, 5)
        self.button.connect("clicked", self.on_modify)

    def set_active(self, value):
        self.check.set_active(value)

    def is_active(self):
        return(self.check.get_active())

    def on_modify(self, widget):
        self.emit("modify_signal", self.location)
