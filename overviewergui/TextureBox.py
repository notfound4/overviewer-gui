import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GLib


class TextureBox(Gtk.Box):

    def __init__(self, parent):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=6)

        self.parent = parent

        labelDir = Gtk.Label("Choose texture pack", justify=Gtk.Justification.LEFT, xalign=0)
        self.pack_start(labelDir, False, False, 0)

        button = Gtk.RadioButton.new_with_label_from_widget(None, "Default")
        button.connect("toggled", self.on_button_toggled, "Default")
        self.pack_start(button, False, False, 0)

        self.buttons = {"Default": button}

        if os.path.isdir(os.path.join(GLib.get_home_dir(), '.minecraft/resourcepacks/')):
            for r in os.listdir(os.path.join(GLib.get_home_dir(), '.minecraft/resourcepacks/')):
                button = Gtk.RadioButton.new_with_label_from_widget(self.button, r)
                button.connect("toggled", self.on_button_toggled, r)
                self.pack_start(button, False, False, 0)
                self.buttons[r] = button

    def reset(self):
        self.texture = self.parent.folderConfig.texture
        self.buttons[self.texture].set_active(True)

    def on_button_toggled(self, widget, texture):
        self.texture = texture

    def commitChange(self):
        self.parent.folderConfig.texture = self.texture
