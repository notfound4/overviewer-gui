import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GObject


class FolderItem(Gtk.Box):
    __gsignals__ = {
        'delete_signal': (GObject.SIGNAL_RUN_FIRST, None, (str,))
    }

    def __init__(self, location):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL, spacing=6)

        self.location = location

        self.check = Gtk.CheckButton.new_with_label(location)
        self.pack_start(self.check, True, True, 5)

        image = Gtk.Image(stock=Gtk.STOCK_DELETE)
        self.button = Gtk.Button(image=image)
        self.pack_end(self.button, False, False, 5)
        self.button.connect("clicked", self.on_delete)

    def on_delete(self, widget):
        self.emit("delete_signal", self.location)

    def set_active(self, value):
        self.check.set_active(value)

    def is_active(self):
        return(self.check.get_active())
