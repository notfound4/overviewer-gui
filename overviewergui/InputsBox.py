import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from FolderItem import FolderItem


class InputsBox(Gtk.Box):

    def __init__(self, parent):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.parent = parent

        label = Gtk.Label("Select the directories where to look for worlds",
                          justify=Gtk.Justification.LEFT, xalign=0)
        self.pack_start(label, False, False, 0)

        self.listboxDir = Gtk.ListBox()
        self.listboxDir.set_selection_mode(Gtk.SelectionMode.NONE)
        self.pack_start(self.listboxDir, True, True, 0)

        add_button = Gtk.Button("Add folder")
        self.pack_start(add_button, False, False, 0)
        add_button.connect("clicked", self.on_add)

    def reset(self):
        self.dict = {}
        for b in self.listboxDir.get_children():
            self.listboxDir.remove(b)

        for d, c in self.parent.folderConfig.inputPaths.items():
            if os.path.isdir(d):
                self.dict[d] = Gtk.ListBoxRow()
                hbox = FolderItem(d)
                self.dict[d].add(hbox)
                self.listboxDir.add(self.dict[d])
                hbox.connect("delete_signal", self.on_delete)
                self.dict[d].show_all()
                hbox.set_active(c)

    def on_delete(self, widget, location):
        self.listboxDir.remove(self.dict[location])
        del self.dict[location]

    def on_add(self, widget):
        dialog = Gtk.FileChooserDialog("Please choose a folder", self.parent,
                                       Gtk.FileChooserAction.SELECT_FOLDER,
                                       (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                        "Select", Gtk.ResponseType.OK))
        dialog.set_default_size(800, 400)
        dialog.set_modal(True)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            loc = dialog.get_filename()
            self.dict[loc] = Gtk.ListBoxRow()
            hbox = FolderItem(loc)
            self.dict[loc].add(hbox)
            self.listboxDir.add(self.dict[loc])
            hbox.connect("delete_signal", self.on_delete)
            self.dict[loc].show_all()
            hbox.set_active(True)

        dialog.destroy()

    def commitChange(self):
        folders = dict()
        for b in self.listboxDir.get_children():
            folders[b.get_children()[0].location] = b.get_children()[0].is_active()

        self.parent.folderConfig.inputPaths = folders
